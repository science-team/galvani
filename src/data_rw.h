/*
 * data_rw.h
 * This file is part of galvani.
 * Copyright (C) 2020-2024 Burkard Lutz <b.lutz@online.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATA_RW_H
#define DATA_RW_H

#include "typedef.h"


//extern Variables:
extern int mi;	//index for actual value
extern bool data_saved;
extern std::string fehlercode;
extern Gtk::MessageDialog* msg_dialog;
extern Gtk::Dialog* projekt_prop;
extern Gtk::Button* projekt_prop_ok;
extern Gtk::Dialog* map_func;
extern Gtk::Button* projekt_prop_cancel;
extern Gtk::MenuItem* menu_eval_data;
extern bool eval_integration;
extern bool eval_regression;
extern bool eval_diff;
extern bool mess_done;
extern double tit_volspeed;
extern std::vector<std::string> m_functions;

extern std::string datadir, tmpdir;

//Pointer to text entries
	extern Gtk::Entry*	projekt_title;
	extern Gtk::ComboBoxText* func_x;
	extern Gtk::ComboBoxText* func_y;
	extern Gtk::TextView* projekt_comment_view;
	extern Glib::RefPtr<Gtk::TextBuffer> projekt_comment;
	extern Gtk::Entry*	file_function_x;
	extern Gtk::Entry*	file_function_y;
	extern Gtk::ComboBoxText* function_x;
	extern Gtk::ComboBoxText* function_y;

extern Gtk::FileChooserDialog* file_dialog;
extern Gtk::Button* file_choose_ok;
extern Gtk::Button* file_choose_cancel;
extern std::string version;



//extern variables (classes)
extern std::vector<mw_data> mw;
extern Messung *project;
extern Diagramm* diag;
extern Multimeter dmm[2];
extern acid_data ha;

//Variablen
bool dialog_ok (std::string msg);
Glib::RefPtr<Gtk::FileFilter> filefilter_dat, filefilter_img;
std::ifstream open_file;
std::string filename;
std::string file_id = "galvani_data_file";
Glib::RefPtr<Gdk::Pixbuf> img;

//functions
void fehler(std::string fehler_text); //error message
void reset_mw (); // initialize array
void new_val(mw_data val);		//prepare next value
void init_diag(); // initialize diagram
void define_project ();
void show_diag ();
void export_diag();
void reset_evaloptions ();
std::string read_item(std::string opt_item);
void translate_functions ();
bool is_known_function (std::string funct);
typ get_dmmtyp(std::string t);

//extern functions
double get_dbstr(std::string num_str);
void set_messfunkt (int kanal, std::string funkt);	//set function of channel X/Y


#endif
