/*
 * eval.h
 * This file is part of galvani.
 * Copyright (C) 2020-2024 Burkard Lutz <b.lutz@online.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EVAL_H
#define EVAL_H

#include "typedef.h"

//extern Variables:
extern int mi;	//index for actual value
extern std::string filename;
extern std::string fehlercode;

extern std::vector<mw_data> mw;
extern Diagramm* diag;
extern Messung *project;
extern Gtk::MessageDialog* msg_dialog;
extern Gtk::Dialog* projekt_prop;
extern bool dialog_ok (std::string msg);
extern Gtk::Dialog* evaloptions_dialog;
extern Gtk::Dialog* eval_results;
extern Gtk::TextView* eval_results_view;
extern Gtk::Label* eval_funktion;
extern Gtk::Notebook* evaloptions;
extern Gtk::Switch* evaloption_vol;
extern Gtk::SpinButton* evaloption_volspeed;
extern Gtk::SpinButton* evaloption_minpeakscale;
extern Gtk::SpinButton* evaloption_mindiffscale;
extern Gtk::ComboBoxText* evaloption_xtherm;
extern Gtk::ComboBoxText* evaloption_ytherm;
extern Gtk::Switch* evaloption_xreziproksw;
extern Gtk::Switch* evaloption_yreziproksw;
extern Gtk::Switch* evaloption_xlogsw;
extern Gtk::Switch* evaloption_ylogsw;
extern Gtk::ColorButton* diag_funktcolor;
extern Gtk::Switch* evaloption_reglin;
extern Gtk::Switch* evaloption_integration;
extern Gtk::Switch* evaloption_diff;
extern Gtk::Switch* evaloption_show_baseline;
extern Gtk::Switch* evaloption_show_peakarea;
extern Gtk::Switch* evaloption_show_peaklimits;
extern Gtk::Box* integration_options;
extern Gtk::Label* display_x;
extern Gtk::Label* display_y;
extern Gtk::ToolButton* button_reset_scale;
extern std::vector<peak_data> peaks;	//peaks in Chromatogram;

//Variables
bool eval_titvolumen = false;
bool eval_xreziprok = false;
bool eval_yreziprok = false;
bool eval_xlog = false;
bool eval_ylog = false;
bool eval_reglin = false;
bool eval_integration = false;
bool eval_diff = false;
thermoval eval_xtherm = no;
thermoval eval_ytherm = no;
double tit_volspeed = 0.0166667;	// Titration volume speed in ml/sec = 1 ml/min
double mw_slope, mw_axis;
double peaks_area;
double min_peakscale = 0.1; // criterion for peak noise (percentage of yscale)
double min_diffscale = 5.0; // criterion for derivative noise (percentage of maximum)
std::vector<diffmax> diff;		//maxima of diffential function
int ndiff; 				//number of maxima of differential function
double yb, yb_a0, yb_a1;	// baseline value, intercept and slope
Glib::RefPtr<Gtk::TextBuffer> eval_result_items;	//results of evaluation
Pango::TabArray tabs;
double diff_min, diff_max;		//min and max of differential function


//Funktions
void fehler(std::string fehler_text); //error message
void reset_mw (); // initialize array for measurement values
void show_diag (); // show diagram
void eval_data ();	//evaluate data
void check_xlog (); // check data for x->log x
void check_xreziprok (); // check data for x->1/x
void check_ylog (); // check data for y->log y
void check_yreziprok (); // check data for y->1/y
void show_evaloptions_integration (int page); //show options for peak integration
void init_diag (); //initialize diagram
void reset_diagscale ();
mw_data trafo (mw_data val);	//mathematical transformation of data
double trafo_log (double x);		//logarithm
double trafo_reziprok (double x);	//reciprocal
double trafo_cuni (double x);		//thermocouple copper/constantane
double trafo_nicr (double x);		//thermocouple nickel/chromenickel
void reset_evaloptions ();
void lin_regression ();				//linear regression of data
polyreg_data reg_lin (std::vector<mw_data> &p);		//linear regression of data
polyreg_data poly2_reg (std::vector<mw_data> &p);	//polynom regression of data (second order)
polyreg_data baseline ();			//find baseline, compute slope and intercept
void peak_integration ();			//integration of peaks
void find_peaks ();					//find peaks in measurement data
peak_data get_peakarea (peak_data peak);					//calculate peak area
peak_data peakreg_gauss (unsigned long long int max); //calculating gauss parameter for peak maximum by polynom regression
void diff_funktion ();				//calculate differential function
mw_data mv_av (std::vector<mw_data> &p);	//calculate moving average
mw_data tr_av (std::vector<mw_data> &p);	//calculate triangular average
void show_evalresults ();
std::string eval_funkt (int kanal, std::string funkt);
std::string eval_unit (int kanal, std::string unit);
std::string num_str (double x, int n);	//transforms decimal to string with n decimal places

#endif