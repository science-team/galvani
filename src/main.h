/*
 * main.h
 * This file is part of galvani.
 *
 * Copyright (C) 2020-2024 Burkard Lutz <b.lutz@online.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_H
#define MAIN_H

#include <glibmm/i18n.h>
#include "typedef.h"

//Pointer

	Glib::RefPtr<Gtk::Builder> builder;

//Pointer to windows
	Gtk::Window* main_win = 0;
	Gtk::Notebook* edit_options = 0;
	Gtk::Notebook* evaloptions = 0;
	Gtk::Box* dmm_serial_options[2] = {0,0};
	Gtk::Box* dmm_usb_options[2] = {0,0};
	Gtk::Box* dmm_dmm_options[2] = {0,0};
	Gtk::Box* dmm_device_options = 0;
	Gtk::Box* dmm_control_options[2] = {0,0};
	Gtk::Box* dmm_function_control[2] = {0,0};
	Gtk::Box* integration_options = 0;
	Gtk::Box* tit_options = 0;
	Gtk::Box* projekt_options = 0;
	Gtk::Box* sim_linoptions[2] = {0,0};
	Gtk::Box* sim_expoptions = 0;
	Gtk::Box* sim_chromoptions = 0;
	Gtk::Window* browser = 0;
		
	
//Pointer to menu items
	Gtk::MenuItem* menu_datei_neu = nullptr;
	Gtk::MenuItem* menu_datei_open = nullptr;
	Gtk::MenuItem* menu_datei_save = nullptr;
	Gtk::MenuItem* menu_datei_print = nullptr;
	Gtk::MenuItem* menu_quit = nullptr;
	Gtk::MenuItem* menu_mess_neu = nullptr;
	Gtk::MenuItem* menu_mess_start = nullptr;
	Gtk::MenuItem* menu_mess_start_timer = nullptr;
	Gtk::MenuItem* menu_mess_cancel = nullptr;
	Gtk::MenuItem* menu_mess_stop = nullptr;
	Gtk::MenuItem* menu_mess_delete = nullptr;
	Gtk::MenuItem* menu_eval_data = nullptr;
	Gtk::MenuItem* menu_export_diag = nullptr;
	Gtk::MenuItem* menu_options_diag = nullptr;
	Gtk::MenuItem* menu_options_deviceX = nullptr;
	Gtk::MenuItem* menu_options_deviceY = nullptr;
	Gtk::MenuItem* menu_options_save = nullptr;
	Gtk::MenuItem* menu_options_load = nullptr;
	Gtk::MenuItem* menu_about = nullptr;
	Gtk::MenuItem* menu_help = nullptr;
	Gtk::CheckMenuItem* menu_options_autoload = nullptr;
	Gtk::CheckMenuItem* menu_options_autosave = nullptr;
	Gtk::CheckMenuItem* menu_view_display = nullptr;



//Pointer to Toolbuttons
	Gtk::ToolButton* button_start = nullptr;
	Gtk::ToolButton* button_timer = nullptr;
	Gtk::ToolButton* button_stop = nullptr;
	Gtk::ToolButton* button_cancel = nullptr;
	Gtk::ToolButton* button_reset = nullptr;
	Gtk::ToolButton* button_messung = nullptr;
	Gtk::ToolButton* button_new = nullptr;
	Gtk::ToolButton* button_edit = nullptr;
	Gtk::ToolButton* button_open = nullptr;
	Gtk::ToolButton* button_save = nullptr;
	Gtk::ToolButton* button_export = nullptr;
	Gtk::ToolButton* button_print = nullptr;
	Gtk::ToolButton* button_reset_scale = nullptr;

//Pointer to dialogues
    Gtk::MessageDialog* error_dialog;
	Gtk::MessageDialog* msg_dialog;
	Gtk::AboutDialog* about_dialog;
	Gtk::FileChooserDialog* file_dialog;
	Gtk::Dialog* projekt_prop = 0;
	Gtk::Dialog* map_func = 0;
	Gtk::Dialog* options_dialog = 0;
	Gtk::Dialog* evaloptions_dialog = 0;
	Gtk::Dialog* eval_results = 0;
	Gtk::Dialog* timer_dialog = 0;
	Gtk::FileChooserDialog* dmmoptions_save_dialog;

//Pointer to text entries
	Gtk::Entry*	projekt_title = nullptr;
	Gtk::ComboBoxText* func_x = nullptr;
	Gtk::ComboBoxText* func_y = nullptr;
	Gtk::TextView* projekt_comment_view = nullptr;
	Gtk::TextView* eval_results_view = nullptr;
	Gtk::Label* eval_funktion = nullptr;
	Glib::RefPtr<Gtk::TextBuffer> projekt_comment;
	Gtk::ComboBoxText* diag_symbol = nullptr;
	Gtk::ComboBoxText* diag_graph = nullptr;
	Gtk::ComboBoxText* diag_linestyle = nullptr;
	Gtk::ComboBoxText* diag_symbolstyle = nullptr;
	Gtk::ComboBoxText* dmm_typ[2] = {0,0};
	Gtk::ComboBoxText* dmm_name[2] = {0,0};
	Gtk::ComboBoxText* device_name = nullptr;
	Gtk::ComboBoxText* dmm_simtyp[2] = {0,0};
	Gtk::ComboBoxText* dmm_interface[2] = {0,0};
	Gtk::ComboBoxText* device_interface = nullptr;
	Gtk::ComboBoxText* dmm_baud[2] = {0,0};
	Gtk::ComboBoxText* dmm_bits[2] = {0,0};
	Gtk::ComboBoxText* dmm_flowcontrol[2] = {0,0};
	Gtk::ComboBoxText* dmm_stopbits[2] = {0,0};
	Gtk::ComboBoxText* dmm_parity[2] = {0,0};
	Gtk::ComboBoxText* dmm_funktion[2] = {0,0};
	Gtk::ComboBoxText* dmm_range[2] = {0,0};
	Gtk::ComboBoxText* dmm_DCV[2] = {0,0};
	Gtk::ComboBoxText* dmm_ACV[2] = {0,0};
	Gtk::ComboBoxText* dmm_DCmA[2] = {0,0};
	Gtk::ComboBoxText* dmm_ACmA[2] = {0,0};
	Gtk::ComboBoxText* dmm_DCA[2] = {0,0};
	Gtk::ComboBoxText* dmm_ACA[2] = {0,0};
	Gtk::ComboBoxText* dmm_Res[2] = {0,0};
	Gtk::ComboBoxText* dmm_Cond[2] = {0,0};
	Gtk::ComboBoxText* dmm_pH[2] = {0,0};
	Gtk::ComboBoxText* dmm_Temp[2] = {0,0};
	Gtk::ComboBoxText* evaloption_xtherm = nullptr;
	Gtk::ComboBoxText* evaloption_ytherm = nullptr;
	Gtk::Entry*	file_function_x = nullptr;
	Gtk::Entry*	file_function_y = nullptr;
	Gtk::ComboBoxText* function_x = nullptr;
	Gtk::ComboBoxText* function_y = nullptr;
	Gtk::ComboBoxText* tit_ha = nullptr;
	

//Pointer to numerical entries
	Gtk::SpinButton* diag_xmin = nullptr;
	Gtk::SpinButton* diag_xmax = nullptr;
	Gtk::SpinButton* diag_ymin = nullptr;
	Gtk::SpinButton* diag_ymax = nullptr;
	Gtk::SpinButton* diag_symw = nullptr;
	Gtk::SpinButton* dmm_timeout[2] = {0,0};
	Gtk::SpinButton* dmm_num_bytes[2] = {0,0};
	Gtk::SpinButton* dmm_usb_bytes[2] = {0,0};
	Gtk::SpinButton* dmm_num_val[2] = {0,0};
	Gtk::SpinButton* dmm_mtime = nullptr;
	Gtk::SpinButton* evaloption_volspeed = nullptr;
	Gtk::SpinButton* evaloption_minpeakscale = nullptr;
	Gtk::SpinButton* evaloption_mindiffscale = nullptr;
	Gtk::SpinButton* timer_start = nullptr;
	Gtk::SpinButton* tit_haconc = nullptr;
	Gtk::SpinButton* tit_baseconc = nullptr;
	Gtk::SpinButton* tit_havol = nullptr;
	Gtk::SpinButton* tit_speed = nullptr;
	Gtk::SpinButton* sim_slope[2] = {0,0};
	Gtk::SpinButton* sim_shift[2] = {0,0};
	Gtk::SpinButton* sim_init = 0;
	Gtk::SpinButton* sim_tc = 0;
	Gtk::SpinButton* sim_expshift = 0;
	Gtk::SpinButton* sim_np = 0;
	Gtk::SpinButton* sim_max_rt = 0;
	Gtk::SpinButton* sim_bf = 0;
	Gtk::SpinButton* sim_drift = 0;
	
	

//Pointer to Button
	Gtk::Button* file_choose_ok;
	Gtk::Button* file_choose_cancel;
	Gtk::Button* projekt_prop_ok;
	Gtk::Button* projekt_prop_cancel;
	Gtk::Button* options_ok;
	Gtk::Button* device_x;
	Gtk::Button* device_y;
	Gtk::Button* options_cancel;
	Gtk::Button* options_reset;
	Gtk::Button* start_timer;
	Gtk::Button* cancel_timer;
	Gtk::ColorButton* diag_bkcolor;
	Gtk::ColorButton* diag_axcolor;
	Gtk::ColorButton* diag_gridcolor;
	Gtk::ColorButton* diag_xycolor;
	Gtk::ColorButton* diag_funktcolor;
	Gtk::Switch* diag_gridsw;
	Gtk::Switch* diag_autoscalesw;
	Gtk::Switch* diag_zerosw;
	Gtk::Switch* diag_titlesw;
	Gtk::Switch* dmm_polling[2];
	Gtk::Button* dmm_loadoptions[2];
	Gtk::Button* dmm_saveoptions[2];
	Gtk::Button* dmmoptions_save_ok;
	Gtk::Button* dmmoptions_save_cancel;
	Gtk::Switch* dmm_remote_control[2];
	Gtk::Switch* dmm_scpi[2];
	Gtk::Switch* evaloption_vol;
	Gtk::Switch* evaloption_xreziproksw;
	Gtk::Switch* evaloption_yreziproksw;
	Gtk::Switch* evaloption_xlogsw;
	Gtk::Switch* evaloption_ylogsw;
	Gtk::Switch* evaloption_reglin;
	Gtk::Switch* evaloption_integration;
	Gtk::Switch* evaloption_diff;
	Gtk::ProgressBar* count_down_progress;
	Gtk::Switch* evaloption_show_baseline;
	Gtk::Switch* evaloption_show_peakarea;
	Gtk::Switch* evaloption_show_peaklimits;
	Gtk::Switch* sim_slope_switch[2];
	Gtk::Switch* sim_shift_switch[2];
	Gtk::Switch* sim_init_sw;
	Gtk::Switch* sim_tc_sw;
	Gtk::Switch* sim_expshift_sw;
	Gtk::Switch* sim_np_sw;
	Gtk::Switch* sim_max_rt_sw;
	Gtk::Switch* sim_bf_sw;
	Gtk::Switch* sim_drift_sw;
	

//Pointer to Label
	Gtk::Label* display_x;
	Gtk::Label* display_y;
	Gtk::Label* count_down_start;


//Variables
std::string version;
Messung *project;
PrintOP *printjob;
std::vector<mw_data> mw;
Multimeter dmm[2];
Multimeter idmm[10];	//array of installed dmm
int mid=0;	//max. index of installed dmm
Diagramm* diag=0;
std::time_t datum;
bool data_saved=true;
bool project_defined=false;
bool autosave=false;
bool autoload = false;
bool show_display = true;
bool full_screen = true;
int mainwin_width, mainwin_height;
std::string fehlercode;
std::string homedir, datadir, workdir, confdir, tmpdir;
std::fstream ini_file;
std::string ini_filename;
std::vector<std::string> m_functions; 
std::vector<acid_data> acid;


//Funktions
void new_project ();	//new project
void define_project (); //set projekt settings
void show_settings_device_x ();	// show project settings for channel X
void show_settings_device_y ();	// show project settings for channel Y
void configure_settings_device_x();	// configure options for channel X and show in project
void configure_settings_device_y();	// configure options for channel Y and show in project
void begin_mess(); //new measurement
void cancel_mess();		//cancel measurement
void start_mess(); //start measurement
void start_mess_with_timer();		//start measurement with timer
void stop_mess(); //stop measurement
void end_mess(); //end measurement and rearrange
void save_data();	//save data
void open_datafile(); //open data file
void export_diag();	//export diagram
void print_report();	//print report
bool dialog_ok (std::string msg); //dialog: confirm action
void app_quit();	//quit program
void eval_data();	//evaluate data
void show_evaloptions_integration (int page); //set options for peak integration
void show_tit_options();		//show options for simulation of titration curve
void set_options();	//set options
void show_options_diag ();	//show options for diagram
void show_options_device_x ();	//options for channel X 
void show_options_device_y ();	//options for channel Y
void set_range_options_device (int k);	//set range options according to function
void show_diag_manual_limits (bool manual);
void show_dmm_control_options (bool state, int kanal); // signal expects function with 2 arguments, state is unused
void show_dmm_function_control(bool state, int k);	// signal expects function with 2 arguments, state is unused
void show_dmm_interface_options(int kanal);
void show_simulation_options (int k);
void show_simlin_options (bool state, int k); // show options for linear simulation
void show_simexp_options (int k); // show options for exponential simulation
void show_simchrom_options (int k); // show options for peak simulation (chromatogram)
void restart_options_x ();
void restart_options_y ();
void preset_mtime ();	//preset measuring timeout according to device option
void select_dmm (int k);
void fehler(std::string fehler_text); //error message
void save_dmm_options(int k);
void load_dmm_options(int k);
void save_all_options ();
void load_all_options ();
std::string read_ini(std::string opt_item);
void toggle_display ();
void reset_diagscale ()
{
	diag->reset_diagscale ();
}
void about ();
void help ();
extern int get_intstr(std::string num_str);

#endif
