/*
 * mess.h
 *
 * This file is part of galvani.
 * 
 * Copyright (C) 2020-2024 Burkard Lutz <b.lutz@online.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MESS_H
#define MESS_H

#include "typedef.h"


//Variablen

extern Gtk::MenuItem* menu_mess_neu;
extern Gtk::MenuItem* menu_mess_start;
extern Gtk::MenuItem* menu_mess_start_timer;
extern Gtk::MenuItem* menu_mess_cancel;
extern Gtk::MenuItem* menu_mess_stop;
extern Gtk::MenuItem* menu_mess_delete;
extern Gtk::MenuItem* menu_eval_data;
extern Gtk::MenuItem* menu_options_diag;
extern Gtk::MenuItem* menu_options_deviceX;
extern Gtk::MenuItem* menu_options_deviceY;
extern Gtk::ToolButton* button_start;
extern Gtk::ToolButton* button_timer;
extern Gtk::ToolButton* button_stop;
extern Gtk::ToolButton* button_cancel;
extern Gtk::ToolButton* button_reset;
extern Gtk::ToolButton* button_messung;
extern Gtk::ToolButton* button_reset_scale;
extern Gtk::Dialog* timer_dialog;
extern Gtk::Button* start_timer;
extern Gtk::Button* cancel_timer;
extern Gtk::Button* device_x;
extern Gtk::Button* device_y;
extern Gtk::Box* projekt_options;
extern Gtk::Box* tit_options;
extern Gtk::ProgressBar* count_down_progress;
extern Gtk::Label* count_down_start;
extern Gtk::SpinButton* timer_start;
extern Gtk::MessageDialog* error_dialog;
extern Gtk::MessageDialog* delete_dialog;
extern Gtk::Entry*	projekt_title;
extern Gtk::ComboBoxText* func_x;
extern Gtk::ComboBoxText* func_y;
extern Gtk::TextView* projekt_comment_view;
extern Glib::RefPtr<Gtk::TextBuffer> projekt_comment;
extern Gtk::Dialog* projekt_prop;
extern Gtk::MessageDialog* msg_dialog;
extern Gtk::Label* display_x;
extern Gtk::Label* display_y;
extern Gtk::ComboBoxText* tit_ha;
extern Gtk::SpinButton* tit_haconc;
extern Gtk::SpinButton* tit_baseconc;
extern Gtk::SpinButton* tit_havol;
extern Gtk::SpinButton* tit_speed;
extern Gtk::CheckMenuItem* menu_view_display;
extern Diagramm* diag;
extern std::time_t datum;
extern Messung *project;
extern std::vector<mw_data> mw;
extern Multimeter dmm[2];
extern bool data_saved;
extern bool project_defined;
extern bool show_display;
extern std::string fehlercode;
extern std::string tmpdir;
extern double mw_slope;
extern double mw_axis;
extern double tit_volspeed;
extern bool eval_reglin;
extern bool eval_diff;
extern bool eval_integration;
extern bool eval_titvolumen;
extern double diff_min, diff_max;
extern double peaks_area;
extern std::vector<diffmax> diff;		//maxima of diffential function
extern int ndiff; 				//number of maxima of differential function
extern double ps;		//scale factor for printing lines in diagram
extern double pf;		//scale factor for printing lines in diagram
extern double yb, yb_a0, yb_a1;	// baseline value, intercept and slope


//gobal Variables
bool mess_done = false;	// measurement done
unsigned long long int mi=0;	//index for actual value


//Variables
GTimer* mtimer;		//timer for measurement
double mt;			//time variable for measurement in seconds
gulong* microsec;	//mikroseconds of time variable (not used)
std::vector<peak_data> peaks;	//peaks in Chromatogram
sigc::connection countdown_timeout;	//measurement timeout: timeout-handler
double timer_cd;	//timer count-down for starting measurement with timer
bool save_autoscale;
double xmin_reset, ymin_reset, xmax_reset, ymax_reset;
std::vector<mw_data> tit_data;	//titration data
acid_data ha;	//acid for simulation of titration curve
int ai; 	//index of acid for simulation of titration curve
extern std::vector<acid_data> acid;

//Functions
void new_project ();	//new project
void define_project(); 	//set project settings
void show_settings_device_x ();	// show project settings for channel X
void show_settings_device_y ();	// show project settings for channel Y
void show_options_device_x(); // show options for channel X
void configure_settings_device_x();	// configure options for channel X and show in project
void show_options_device_y(); // show options for channel Y
void configure_settings_device_y();	// configure options for channel Y and show in project
void show_tit_options();		//show options for simulation of titration curve
int get_acid_index(std::string name);	// get index of acid for simulation of titration curve
void set_messfunkt (int kanal, std::string funkt); //set function of channel X/Y
std::string set_std_unit (std::string funkt);	//set standard unit
std::string set_std_range (std::string funkt);	//set standard range
void reset_mw (); 		//initialize array
void init_diag(); 		//initialize diagram area
void begin_mess();		//new measurement
void cancel_mess();		//cancel measurement
void start_mess();		//start measurement
void start_mess_with_timer();		//start measurement with timer
void get_val();		// get measurement value
void new_val(mw_data val);		//prepare next value
void show_countdown ();
bool show_countdown_progress ();
void stop_mess();		//stop measurement
void end_mess();		//end measurement and rearrange 
void fehler(std::string fehler_text); //error message
bool dialog_ok (std::string msg);
void save_data();		//save data
void display_mw (m_record xval, m_record yval);	//display numerical values on screen
void toggle_display ();
mw_data trafo (mw_data val);
std::string eval_funkt(int kanal, std::string funkt);
std::string eval_unit (int kanal, std::string unit);
mw_data mv_av (std::vector<mw_data> &p);	//calculate moving average
mw_data tr_av (std::vector<mw_data> &p);	//calculate triangular average
void find_xy_minmax ();
void reset_evaloptions ();
void reset_diagscale ();
double get_dbstr(std::string num_str);
int get_intstr(std::string num_str);
polyreg_data poly3_reg (std::vector<mw_data> &p);	//polynom regression of data (third order)


#endif
