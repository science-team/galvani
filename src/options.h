/*
 * options.h
 * This file is part of galvani.
 * Copyright (C) 2020-2024 Burkard Lutz <b.lutz@online.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPTIONS_H
#define OPTIONS_H

#include "typedef.h"

// extern Variables
extern Diagramm* diag;
extern Messung *project;
extern Gtk::Dialog* options_dialog;
extern Gtk::Button* options_ok;
extern Gtk::Button* options_cancel;
extern Gtk::Button* options_reset;
extern Gtk::Notebook* edit_options;
extern Gtk::ColorButton* diag_bkcolor;
extern Gtk::ColorButton* diag_axcolor;
extern Gtk::ColorButton* diag_gridcolor;
extern Gtk::ColorButton* diag_xycolor;
extern Gtk::Switch* diag_gridsw;
extern Gtk::Switch* diag_autoscalesw;
extern Gtk::Switch* diag_zerosw;
extern Gtk::Switch* diag_titlesw;
extern Gtk::ComboBoxText* diag_symbol;
extern Gtk::ComboBoxText* diag_graph;
extern Gtk::ComboBoxText* diag_linestyle;
extern Gtk::ComboBoxText* diag_symbolstyle;
extern Gtk::SpinButton* diag_xmin;
extern Gtk::SpinButton* diag_xmax;
extern Gtk::SpinButton* diag_ymin;
extern Gtk::SpinButton* diag_ymax;
extern Gtk::SpinButton* diag_symw;
extern Gtk::Box* dmm_serial_options[2];
extern Gtk::Box* dmm_usb_options[2];
extern Gtk::Box* dmm_dmm_options[2];
extern Gtk::Box* dmm_device_options;
extern Gtk::Box* dmm_control_options[2];
extern Gtk::Box* dmm_function_control[2];
extern Gtk::ComboBoxText* dmm_typ[2];
extern Gtk::ComboBoxText* dmm_name[2];
extern Gtk::ComboBoxText* device_name;
extern Gtk::ComboBoxText* dmm_simtyp[2];
extern Gtk::ComboBoxText* dmm_interface[2];
extern Gtk::ComboBoxText* device_interface;
extern Gtk::ComboBoxText* dmm_baud[2];
extern Gtk::ComboBoxText* dmm_bits[2];
extern Gtk::ComboBoxText* dmm_flowcontrol[2];
extern Gtk::ComboBoxText* dmm_stopbits[2];
extern Gtk::ComboBoxText* dmm_parity[2];
extern Gtk::Switch* dmm_polling[2];
extern Gtk::SpinButton* dmm_num_bytes[2];
extern Gtk::SpinButton* dmm_usb_bytes[2];
extern Gtk::SpinButton* dmm_num_val[2];
extern Gtk::SpinButton* dmm_timeout[2];
extern Gtk::ComboBoxText* dmm_funktion[2];
extern Gtk::ComboBoxText* dmm_range[2];
extern Gtk::SpinButton* dmm_mtime;
extern Gtk::Button* dmm_loadoptions[2];
extern Gtk::Button* dmm_saveoptions[2];
extern Gtk::FileChooserDialog* dmmoptions_save_dialog;
extern Gtk::Button* dmmoptions_save_ok;
extern Gtk::Button* dmmoptions_save_cancel;
extern Gtk::Switch* dmm_remote_control[2];
extern Gtk::Box* dmm_control_options[2];
extern Gtk::Switch* dmm_scpi[2];
extern Gtk::ComboBoxText* dmm_DCV[2];
extern Gtk::ComboBoxText* dmm_ACV[2];
extern Gtk::ComboBoxText* dmm_DCmA[2];
extern Gtk::ComboBoxText* dmm_ACmA[2];
extern Gtk::ComboBoxText* dmm_DCA[2];
extern Gtk::ComboBoxText* dmm_ACA[2];
extern Gtk::ComboBoxText* dmm_Res[2];
extern Gtk::ComboBoxText* dmm_Cond[2];
extern Gtk::ComboBoxText* dmm_pH[2];
extern Gtk::ComboBoxText* dmm_Temp[2];
extern Gtk::Box* sim_linoptions[2];
extern Gtk::Box* sim_expoptions;
extern Gtk::Box* sim_chromoptions;
extern Gtk::Switch* sim_slope_switch[2];
extern Gtk::Switch* sim_shift_switch[2];
extern Gtk::SpinButton* sim_slope[2];
extern Gtk::SpinButton* sim_shift[2];
extern Gtk::Switch* sim_init_sw;
extern Gtk::Switch* sim_tc_sw;
extern Gtk::Switch* sim_expshift_sw;
extern Gtk::SpinButton* sim_init;
extern Gtk::SpinButton* sim_tc;
extern Gtk::SpinButton* sim_expshift;
extern Gtk::Switch* sim_np_sw;
extern Gtk::Switch* sim_max_rt_sw;
extern Gtk::Switch* sim_bf_sw;
extern Gtk::Switch* sim_drift_sw;
extern Gtk::SpinButton* sim_np;
extern Gtk::SpinButton* sim_max_rt;
extern Gtk::SpinButton* sim_bf;
extern Gtk::SpinButton* sim_drift;


extern std::string workdir;
extern std::string confdir;
extern std::string fehlercode;

extern Multimeter dmm[2];
extern Multimeter idmm[10];
extern int mid;
extern bool mess_done;
extern bool eval_titvolumen;


//Functions
bool dialog_ok (std::string msg);
void fehler(std::string fehler_text); //error message
void set_options();	//set options
void show_options_diag ();	//show options for diagram
void show_options_device_x ();	//settings for channel X 
void show_options_device_y ();	//settings for channel Y
void set_range_options_device (int k);	//set range options according to function
void show_diag_settings ();	//show actual settings
void show_options_Kanal_XY ();
void show_diag_manual_limits (bool manual);
void show_dmm_control_options (bool state, int kanal); // signal expects function with 2 arguments, state is unused
void show_simulation_options (int k);
void show_simlin_options (bool state, int k); // show options for linear simulation
void show_simexp_options (int k); // show options for exponential simulation
void show_simchrom_options (int k); // show options for peak simulation (chromatogram)
void set_options_diag ();	//set options for diagram
void set_device_options (int k);	//set options for channel X,Y
void restart_options_x ();
void restart_options_y ();
void reset_options_diag ();
void reset_options_kanal_X ();
void reset_options_kanal_Y ();
bool dmm_exist (std::string name);	// seeks name in  array of installed dmms; sets index if found
void select_dmm (int k);
void save_dmm_options(int k);
void load_dmm_options(int k);
std::string read_dmmoption(std::string opt_item);
void save_all_options ();
void load_all_options ();
void translate_functions ();
bool is_known_function (std::string funct);
std::string read_option(std::string opt_item);
double get_dbstr(std::string num_str);
int get_intstr(std::string num_str);
bool get_boolstr(std::string num_str);
typ get_dmmtyp(std::string t);
void show_dmm_interface_options(int kanal);
void show_dmm_function_control(bool state, int k);
void set_messfunkt (int kanal, std::string funkt); //set function for channel X/Y
std::string set_std_unit (std::string funkt);	//set standard unit
std::string set_std_range (std::string funkt);	//set standard range
void set_device_options_lutron_ph207 ();
void preset_mtime ();	//preset measuring timeout according to device option
void set_mtime ();	//set measuring timeout according to device
std::vector<std::string> get_usb_ports ();

//Variables
Gdk::RGBA std_axcol, std_gridcol, std_bkcol, std_xycol;
double messintervall;
int min_timeout;
Glib::RefPtr<Gtk::FileFilter> filefilter_dmm;
std::string dmm_file_id = "galvani_dmm_options";
std::string conf_file_id = "galvani_conf_options";
std::ifstream dmm_open_file;
std::ifstream conf_open_file;
std::vector<std::string> ports;
int id;	//index of found dmm in array


#endif