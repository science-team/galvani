/*
 * print.h
 * This file is part of galvani.
 * Copyright (C) 2020-2024 Burkard Lutz <b.lutz@online.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRINT_H
#define PRINT_H

#include "typedef.h"

//extern variables
extern bool eval_integration;
extern bool eval_reglin;
extern bool eval_diff;
extern Messung *project;
extern Diagramm* diag;
extern PrintOP *printjob;

//extern functions
void fehler(std::string fehler_text); //error message
void print_report();	//print report

//variables
double ps = 1.0;		//scale factor for printing lines in diagram
double pf = 1.0;		//scale factor for printing fonts in diagram

#endif